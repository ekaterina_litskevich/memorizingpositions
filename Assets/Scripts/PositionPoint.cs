﻿using UnityEngine;
using TMPro;

public class PositionPoint : MonoBehaviour
{
    [SerializeField] private TextMeshPro textPositionNumber;
    public void ChangeText(int Number)
    {
        textPositionNumber.text = string.Format("{0}", Number);
    }

    public void ChangeNumber()
    {
        string numberText = textPositionNumber.text;
        int number = int.Parse(numberText);
        number--;

        textPositionNumber.text = string.Format("{0}", number); 

        if (number <= 0)
        {
            Destroy(gameObject);
        }
    }
}
