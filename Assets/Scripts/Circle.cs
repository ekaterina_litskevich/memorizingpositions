﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Circle : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private Slider sliderSpeed;
    [SerializeField] private GameObject parentPositionPoints;
    [SerializeField] private PositionPoint positionPointPrefab;

    private Queue<PositionPoint> positionPoints = new Queue<PositionPoint>();

    private bool isMove = false;

    private float speed = 1.0f;
    private float newSpeed = 1.0f;

    private int numberPosition = 0;

    PositionPoint m_positionPoint;

    private void Start()
    {
        sliderSpeed.onValueChanged.AddListener(ChangeSpeed);
    }
    private void ChangeSpeed(float value)
    {
        newSpeed = value;
    }

    private IEnumerator MoveCoroutine()
    {
        for (int i = 0; i < positionPoints.Count;)
        {
            speed = newSpeed;

            if (m_positionPoint == null)
                m_positionPoint = positionPoints.Dequeue();

            float distance = Vector3.Distance(transform.position, new Vector3(m_positionPoint.transform.position.x, m_positionPoint.transform.position.y, 0));
            float motionMove = distance / speed;

            yield return new WaitForSeconds(motionMove);

            numberPosition--;

            m_positionPoint.ChangeNumber();
            foreach (PositionPoint j in positionPoints)
            {
                j.ChangeNumber();
            }

            m_positionPoint = null;
        }

        isMove = false;
    }

    private void CreateNextPosition()
    {
        numberPosition++;

        Vector3 nextPosition = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1));
        PositionPoint positionPoint = Instantiate(positionPointPrefab, parentPositionPoints.transform);
        positionPoint.transform.position = nextPosition;
        positionPoint.ChangeText(numberPosition);
        positionPoints.Enqueue(positionPoint);
    }

    private void OnDisable()
    {
        sliderSpeed.onValueChanged.RemoveListener(ChangeSpeed);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        { 
            if (!isMove)
            {
                CreateNextPosition();
                StartCoroutine(MoveCoroutine());

                isMove = true;
            }
            else
            {
                CreateNextPosition();
            }
         }

        if (isMove)
        {
            transform.position = Vector2.MoveTowards(transform.position, m_positionPoint.transform.position, speed * Time.deltaTime);
        }
    }
}
